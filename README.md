# Future Nintendo

#Project Description
"This game features an Authentication System with Register, Login, and Reset Password functions, and serves as a final project for a class on data structures, including implementation of Stack, Queue, and Linked List."

#About the Authors
John Carlo Diga
Johnmark Faeldonia

#Installation Guidelines
pip install fontstyle
pip install colorama
python3 -m pip install --upgrade termcolor


#To use this game:

1. Find the file path and play.
2. Please register your username and password before starting.
3. Enter your registered username and password to login.
4. Select a game you would like to play.
5. When you are finished playing, please use the "game over" feature to exit and return to the beginning.
6. Enjoy!"

#Project Documentation
Our Projects works properly


   
