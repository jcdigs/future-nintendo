import math
import os
import fontstyle
import time

from tqdm import tqdm , trange
from colorama import init
from termcolor import colored

playing = True

while(playing):

    print(fontstyle.apply('::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::', 'yellow'))
    print(fontstyle.apply('::              __  ____                                                          ::', 'yellow'))
    print(fontstyle.apply('::             /  |/  (_)___  ___  ______      _____  ___  ____  ___  _____       ::', 'yellow'))
    print(fontstyle.apply('::            / /|_/ / / __ \/ _ \/ ___/ | /| / / _ \/ _ \/ __ \/ _ \/ ___/       ::', 'yellow'))
    print(fontstyle.apply('::           / /  / / / / / /  __(__  )| |/ |/ /  __/  __/ /_/ /  __/ /           ::', 'yellow'))
    print(fontstyle.apply('::          /_/  /_/_/_/ /_/\___/____/ |__/|__/\___/\___/ .___/\___/_/            ::', 'yellow'))
    print(fontstyle.apply('::                                                     /_/                        ::', 'yellow'))
    print(fontstyle.apply('::                         [B] To back to game selection                          ::', 'yellow'))
    print(fontstyle.apply('::                                                                                ::', 'yellow'))
    print(fontstyle.apply('::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::', 'yellow'))

    userInput = input(fontstyle.apply("Type M to Play: ", "bold/green"))

    if userInput == "M":
       os.system('cls')
       print(fontstyle.apply('Minesweeper is online! Goodluck!','green'))
       playing = os.system('python minesweeper.py')
    if userInput == "B":
       os.system('cls')
       print(fontstyle.apply('Back to game selection!','green'))
       playing = os.system('python games.py')
    elif userInput != playing:
       os.system('cls')
       break