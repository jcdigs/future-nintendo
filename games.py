import os
import fontstyle

from tqdm import tqdm , trange
from colorama import init
from termcolor import colored

playing = True

while(playing):

    print(fontstyle.apply('::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::', 'yellow'))
    print(fontstyle.apply('::                          ______                                                ::', 'yellow'))
    print(fontstyle.apply('::                         / ____/___ _____ ___  ___  _____                       ::', 'yellow'))
    print(fontstyle.apply('::                        / / __/ __ `/ __ `__ \/ _ \/ ___/                       ::', 'yellow'))
    print(fontstyle.apply('::                       / /_/ / /_/ / / / / / /  __(__  )                        ::', 'yellow'))
    print(fontstyle.apply('::                       \____/\__,_/_/ /_/ /_/\___/____/                         ::', 'yellow'))
    print(fontstyle.apply('::                                                                                ::', 'yellow'))
    print(fontstyle.apply('::                                [1] 4 in a Row                                  ::', 'yellow'))
    print(fontstyle.apply('::                                [2] Hangman                                     ::', 'yellow'))
    print(fontstyle.apply('::                                [3] Minesweeper                                 ::', 'yellow'))
    print(fontstyle.apply('::                                                                                ::', 'yellow'))
    print(fontstyle.apply('::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::', 'yellow'))

    userInput = input(fontstyle.apply("Choose from 1-3 or type E to exit: ", "bold/green"))

    if userInput == "1":
       os.system('cls')
       playing = os.system('python main_fourrow.py')
    if userInput == "2":
       os.system('cls')
       playing = os.system('python main_hangman.py')
    if userInput == "3":
       os.system('cls')
       playing = os.system('python main_minesweeper.py')
    if userInput == "E":
       os.system('cls')
       print(fontstyle.apply("You have been return to registration page!",'yellow'))
       break
    elif userInput != playing:
       os.system('cls')
       break