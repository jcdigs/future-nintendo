import random
import os
import sys
import fontstyle
from tqdm import tqdm , trange
from colorama import init
from termcolor import colored
from words import words
import string 
from hangman_visual import lives_visual

def get_valid_word(words):

    word = random.choice(words)
    while '-' in word or ' ' in word:
        word = random.choice(words)

    return word.upper()

def hangman():
    word = get_valid_word(words)
    word_letters = set(word) 
    alphabet = set(string.ascii_uppercase)
    used_letters = set() 
    lives = 7
    
    
    while len(word_letters) > 0 and lives > 0:
        
        print("You have", lives, "lives left and You have used these letters: ", " ".join(used_letters))
        print("Type 1 to back to game selection.")

        word_list = [letter if letter in used_letters else '-' for letter in word]
        print(lives_visual[lives])
        print("Current word: ", " ".join(word_list))

        user_letter = input("Guess a letter: ").upper()

        if user_letter == "1":
            os.system('cls')
            os.system('python games.py')
            sys.exit()

        if user_letter in alphabet - used_letters:
            used_letters.add(user_letter)
            if user_letter in word_letters:
                word_letters.remove(user_letter) 
                print('')
            
            else: lives = lives - 1
        
        elif user_letter in used_letters:
            print(fontstyle.apply("You have already used that character. Try again.",'red'))
        else:
            print(fontstyle.apply("That's not a valid character. Try again.",'red'))

    if lives == 0:
        print(lives_visual[lives])
        print("You have died. The word is: ", word)
        lives = os.system('python games.py')
    else:
        print(fontstyle.apply("You guessed the word! It's:", word,'green'))
        lives = os.system('python games.py')




if __name__ == '__main__':
    hangman()