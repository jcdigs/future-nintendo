import os
import fontstyle
import pwinput
import csv
from tqdm import tqdm , trange
from colorama import init
from termcolor import colored

def register():
    username = input('Enter Username: ')
    password = pwinput.pwinput(prompt='Enter your Password: ', mask='*')
    conf_pwd = pwinput.pwinput(prompt='Confirm Password: ', mask='*')
    if conf_pwd == password:
        with open('database.csv', 'a') as csv_file:
            writer = csv.writer(csv_file)
            writer.writerow([username, password])
        print(fontstyle.apply('You have registered successfully!','green'))
    else:
        print(fontstyle.apply('Password is not same as above! Try again!','red' '\n'))

def login():
    username = input('Username: ')
    password = pwinput.pwinput(prompt='Password: ', mask='*')
    with open('database.csv') as csv_file:
        reader = csv.reader(csv_file)
        is_success = False
        for row in reader:
            if len(row) > 1:
                if row[0] == username and row[1] == password:
                    is_success = True

        if is_success:
            os.system('cls')
            print(fontstyle.apply('Logged in Successfully!','green'))
            username = os.system('python main_screen.py')
        else:
            print(fontstyle.apply('Login failed! Try again!','red' '\n'))

def reset_password():
    print(fontstyle.apply('Login first to reset your Password.','yellow'))
    username = input('Username: ')
    password = pwinput.pwinput(prompt='Password: ', mask='*')
    with open('database.csv') as csv_file:
        reader = csv.reader(csv_file)
        is_success = False
        counter = 0
        index = None
        new_list = []
        for row in reader:
            new_list.append(row)
            if len(row) > 1:
                if row[0] == username and row[1] == password:
                    is_success = True
                    index = counter
            counter += 1
        
        if is_success:
            new_password = input('Input you new Password: ')
            for i in range(len(new_list)):
                if i == index:
                    new_list[i][1] = new_password
            with open('database.csv', 'w+') as csv_file:
                writer = csv.writer(csv_file)
                for i in range(len(new_list)):
                    writer.writerow(new_list[i])
                    os.system('cls')
                    print(fontstyle.apply('Password changed successfully!','green'))
        else:
            print(fontstyle.apply('Login failed! Try again!','red' '\n'))

while 1:
    print(fontstyle.apply('::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::', 'yellow'))
    print(fontstyle.apply('::               ____             _      __             __  _                     ::', 'yellow'))
    print(fontstyle.apply('::              / __ \___  ____ _(_)____/ /__________ _/ /_(_)___  ____           ::', 'yellow'))
    print(fontstyle.apply('::             / /_/ / _ \/ __ `/ / ___/ __/ ___/ __ `/ __/ / __ \/ __ \          ::', 'yellow'))
    print(fontstyle.apply('::            / _, _/  __/ /_/ / (__  ) /_/ /  / /_/ / /_/ / /_/ / / / /          ::', 'yellow'))
    print(fontstyle.apply('::           /_/ |_|\___/\__, /_/____/\__/_/   \__,_/\__/_/\____/_/ /_/           ::', 'yellow'))
    print(fontstyle.apply('::                      /____/                                                    ::', 'yellow'))
    print(fontstyle.apply('::                                                                                ::', 'yellow'))
    print(fontstyle.apply('::                                [1] Register                                    ::', 'yellow'))
    print(fontstyle.apply('::                                [2] Login                                       ::', 'yellow'))
    print(fontstyle.apply('::                                [3] Logout                                      ::', 'yellow'))
    print(fontstyle.apply('::                                [4] Reset Password                              ::', 'yellow'))
    print(fontstyle.apply('::                                                                                ::', 'yellow'))
    print(fontstyle.apply('::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::', 'yellow'))
    
    ch = int(input(fontstyle.apply("Enter your choice: ", "bold/green")))
    if ch == 1:
        register()
    elif ch == 2:
        login()
    elif ch == 3:
        os.system('cls')
        print(fontstyle.apply("Account has been logout! Thank you for playing!",'yellow'))
        break
    elif ch == 4:
        reset_password()
    else:
        print(fontstyle.apply('Error input! Program has been terminated!','red'))
        break