import time
import os
import fontstyle
from tqdm import tqdm , trange
from colorama import init
from termcolor import colored

print(fontstyle.apply('Loading Nintendo...',' green'))
print("")
pbar = tqdm(total = 100)
for i in range(10):
  time.sleep(0.5)
  pbar.update(10)
pbar.close()
os.system('cls')
os.system('python registration.py')