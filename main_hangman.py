import math
import os
import fontstyle
import time

from tqdm import tqdm , trange
from colorama import init
from termcolor import colored

playing = True

while(playing):

    print(fontstyle.apply('::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::', 'yellow'))
    print(fontstyle.apply('::                   __  __                                                       ::', 'yellow'))
    print(fontstyle.apply('::                  / / / /___ _____  ____ _____ ___  ____ _____                  ::', 'yellow'))
    print(fontstyle.apply('::                 / /_/ / __ `/ __ \/ __ `/ __ `__ \/ __ `/ __ \                 ::', 'yellow'))
    print(fontstyle.apply('::                / __  / /_/ / / / / /_/ / / / / / / /_/ / / / /                 ::', 'yellow'))
    print(fontstyle.apply('::               /_/ /_/\__,_/_/ /_/\__, /_/ /_/ /_/\__,_/_/ /_/                  ::', 'yellow'))
    print(fontstyle.apply('::                                 /____/                                         ::', 'yellow'))
    print(fontstyle.apply('::                                                                                ::', 'yellow'))
    print(fontstyle.apply('::                         [B] To back to game selection                          ::', 'yellow'))
    print(fontstyle.apply('::                                                                                ::', 'yellow'))
    print(fontstyle.apply('::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::', 'yellow'))

    userInput = input(fontstyle.apply("Type H to Play: ", "bold/green"))

    if userInput == "H":
       os.system('cls')
       print(fontstyle.apply('Hangman is online! Goodluck!','green'))
       playing = os.system('python hangman.py')
    if userInput == "B":
       os.system('cls')
       print(fontstyle.apply('Back to game selection!','green'))
       playing = os.system('python games.py')
    elif userInput != playing:
       os.system('cls')
       break