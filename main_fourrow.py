import math
import os
import fontstyle
import time

from tqdm import tqdm , trange
from colorama import init
from termcolor import colored

playing = True

while(playing):

    print(fontstyle.apply('::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::', 'yellow'))
    print(fontstyle.apply('::                  __ __     _                   ____                            ::', 'yellow'))
    print(fontstyle.apply('::                 / // /    (_)___     ____ _   / __ \____ _      __             ::', 'yellow'))
    print(fontstyle.apply('::                / // /_   / / __ \   / __ `/  / /_/ / __ \ | /| / /             ::', 'yellow'))
    print(fontstyle.apply('::               /__  __/  / / / / /  / /_/ /  / _, _/ /_/ / |/ |/ /              ::', 'yellow'))
    print(fontstyle.apply('::                 /_/    /_/_/ /_/   \__,_/  /_/ |_|\____/|__/|__/               ::', 'yellow'))
    print(fontstyle.apply('::                                                                                ::', 'yellow'))
    print(fontstyle.apply('::                         [B] To back to game selection                          ::', 'yellow'))
    print(fontstyle.apply('::                                                                                ::', 'yellow'))
    print(fontstyle.apply('::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::', 'yellow'))

    userInput = input(fontstyle.apply("Type 4 to Play: ", "bold/green"))

    if userInput == "4":
       os.system('cls')
       print(fontstyle.apply('4 in a Row is online! Goodluck!','green'))
       playing = os.system('python four_row.py')
    if userInput == "B":
       os.system('cls')
       print(fontstyle.apply('Back to game selection!','green'))
       playing = os.system('python games.py')
    elif userInput != playing:
       os.system('cls')
       break