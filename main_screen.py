import os
import fontstyle

from tqdm import tqdm , trange
from colorama import init
from termcolor import colored

init()

playing = True

while(playing):

    print(fontstyle.apply('::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::', 'yellow'))
    print(fontstyle.apply('::     ______      __                     _   ___       __                 __     ::', 'yellow'))
    print(fontstyle.apply('::    / ____/_  __/ /___  __________     / | / (_)___  / /____  ____  ____/ /___  ::', 'yellow'))
    print(fontstyle.apply('::   / /_  / / / / __/ / / / ___/ _ \   /  |/ / / __ \/ __/ _ \/ __ \/ __  / __ \ ::', 'yellow'))
    print(fontstyle.apply('::  / __/ / /_/ / /_/ /_/ / /  /  __/  / /|  / / / / / /_/  __/ / / / /_/ / /_/ / ::', 'yellow'))
    print(fontstyle.apply(':: /_/    \__,_/\__/\__,_/_/   \___/  /_/ |_/_/_/ /_/\__/\___/_/ /_/\__,_/\____/  ::', 'yellow'))
    print(fontstyle.apply('::                                                                                ::', 'yellow'))
    print(fontstyle.apply('::                              ©Created: 2022ツ                            v2.0  ::', 'yellow'))
    print(fontstyle.apply('::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::', 'yellow'))

    userInput = input(fontstyle.apply("Type S to Start: ", "bold/green"))

    if userInput == "S":
       print(fontstyle.apply('Welcome! Choose your game!','green'))
       playing = os.system('python games.py')
    elif userInput != playing:
       os.system('cls')
       print(fontstyle.apply('Error input! You have been return to registration page!','red'))
       break